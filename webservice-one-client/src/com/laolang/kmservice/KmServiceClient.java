package com.laolang.kmservice;

import com.laolang.kmservice.service.impl.SysMenuServiceImplService;

public class KmServiceClient {

	public static void main(String[] args) {
		SysMenuServiceImplService sysMenuService = new SysMenuServiceImplService();
		String sayHello = sysMenuService.getSysMenuServiceImplPort().sayHello();
		System.out.println(sayHello);
	}
}
