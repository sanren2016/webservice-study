package com.laolang.kmservice.service.impl;

import javax.jws.WebService;

import com.laolang.kmservice.service.SysMenuService;

@WebService(endpointInterface = "com.laolang.kmservice.service.SysMenuService")
public class SysMenuServiceImpl implements SysMenuService {

	@Override
	public String sayHello() {
		return "hello world";
	}

}
