package com.laolang.kmservice;

import javax.xml.ws.Endpoint;

import com.laolang.kmservice.service.impl.SysMenuServiceImpl;

public class KmServiceService {

	public static void main(String[] args) {
		String address = "http://localhost:8093/ns";
		Endpoint.publish(address, new SysMenuServiceImpl());
		System.out.println("启动成功");
	}
}
