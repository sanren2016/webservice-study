package com.laolang.kmservice;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

import com.laolang.kmservice.service.SysMenuService;

public class KmServiceTest {

	public static void main(String[] args) {
		try {
			// 创建访问wsdl服务地址的url
			URL url = new URL("http://localhost:8095/ns?wsdl");
			// 通过QName 知名服务的具体信息
			QName sname = new QName("http://impl.service.kmservice.laolang.com/", "SysMenuServiceImplService");
			// 创建服务
			Service service = Service.create(url, sname);
			// 实现接口
			SysMenuService sysmenuService = service.getPort(SysMenuService.class);
			System.out.println(sysmenuService.sayHello());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}

	}
}
